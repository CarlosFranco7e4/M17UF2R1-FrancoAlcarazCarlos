﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class FollowPlayer : MonoBehaviour
{
    public Transform player;
    private NavMeshAgent enemy;    
    private NavMeshPath path;
    private Animator ani;

    [SerializeField]
    private GameObject restPoint;    
    [SerializeField]
    private float speed;
    [SerializeField]
    private float maxDistance;
    [SerializeField]
    private float stopDistance;    
    
    private bool moving;
    
    public virtual void Start()
    {
        enemy = GetComponent<NavMeshAgent>();
        player = GameObject.Find("MainCharacter").GetComponent<Transform>();
        enemy.SetDestination(player.transform.position);
        path = new NavMeshPath();
        ani = gameObject.GetComponent<Animator>();        

        enemy.speed = speed;
        enemy.stoppingDistance = stopDistance;
        moving = true;        
    }
    
    public virtual void Update()
    {        
        if (moving)
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                moving = false;                
            }

            if (Vector3.Distance(player.position, gameObject.transform.position) <= maxDistance)
            {
                bool bDistance = enemy.remainingDistance >= enemy.stoppingDistance;
                Move(bDistance, player.transform);
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                moving = true;
            }

            if (Vector3.Distance(restPoint.transform.position, gameObject.transform.position) <= maxDistance)
            {
                bool bDistance = enemy.remainingDistance >= enemy.stoppingDistance;
                Move(bDistance, restPoint.transform);
            }
        }
    }    

    private void OnTriggerEnter(Collider collision)
    {        
        if (collision.gameObject.CompareTag("Player"))
        {
            StartCoroutine(Attacking());
        }
    }

    protected void Move(bool bDistance, Transform go)
    {        
        if (!bDistance)
        {
            enemy.isStopped = true;
            ani.SetBool("Run", false);
        }
        else
        {
            enemy.isStopped = false;
            enemy.SetDestination(go.position);
            ani.SetBool("Run", true);
        }
    }

    IEnumerator Attacking()
    {
        ani.SetBool("Attack", true);
        yield return new WaitForSeconds(2);
        ani.SetBool("Attack", false);
    }
}
