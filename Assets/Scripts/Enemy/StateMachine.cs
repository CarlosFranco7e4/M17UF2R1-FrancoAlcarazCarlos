﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class StateMachine : MonoBehaviour
{
    private NavMeshAgent enemy;
    private GameObject objective;    
    private float idleCooldown;    
    private Animator ani;

    [SerializeField] private Transform[] patrollingPoints;
    private int patrollingIndex = 0;

    private Transform Player;    

    public enum states
    {
        Idle,
        Patrolling,
        FollowPlayer,
        Attack
    }
    public states currentState;    

    void Start()
    {
        enemy = GetComponent<NavMeshAgent>();
        currentState = states.Idle;
        Player = GameObject.Find("MainCharacter").GetComponent<Transform>();
        ani = gameObject.GetComponent<Animator>();
    }

    void Update()
    {
        switch (currentState)
        {
            case states.Idle:
                Idle();
                break;

            case states.Patrolling:
                Patrolling();
                break;

            case states.FollowPlayer:
                FollowPlayer();
                break;

            case states.Attack:
                Attack();
                break;
        }

        float dist = Vector3.Distance(Player.position, transform.position);

        if (dist <= 1)
        {
            currentState = states.Attack;
        }
    }

    private void Idle()
    {
        idleCooldown -= Time.deltaTime;        

        if (idleCooldown < 0)
        {
            currentState = states.Patrolling;
        }
    }

    private void Patrolling()
    {
        enemy.speed = 4f;        

        if (!enemy.pathPending && enemy.remainingDistance < 0.5f)
            NextPatrolPoint();
    }

    private void FollowPlayer()
    {
        enemy.speed = 5.5f;
        //ani.ResetTrigger("Attack");        

        if (objective != null)
        {
            enemy.SetDestination(objective.transform.position);
        }
    }

    private void Attack()
    {
        StartCoroutine(Attacking());
    }

    private void NextPatrolPoint()
    {
        if (patrollingPoints.Length == 0) return;

        enemy.destination = patrollingPoints[patrollingIndex].position;

        patrollingIndex = (patrollingIndex + 1) % patrollingPoints.Length;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            currentState = states.FollowPlayer;
            objective = Player.gameObject;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            currentState = states.Patrolling;
            objective = null;
        }
    }

    IEnumerator Attacking()
    {
        ani.SetBool("Attack", true);
        yield return new WaitForSeconds(2);
        ani.SetBool("Attack", false);
        currentState = states.Patrolling;
    }
}
