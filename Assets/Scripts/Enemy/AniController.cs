﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AniController : MonoBehaviour
{    
    private Animator ani;    
    private bool run = true;

    private Vector3 lastPosition;
    private Quaternion lastRotation;

    private void Awake()
    {
        lastPosition = transform.position;
        lastRotation = transform.rotation;
    }

    private void Start()
    {
        ani = gameObject.GetComponent<Animator>(); ;
    }

    private void Update()
    {
        if (run)
        {
            var tr = transform;
            var position = tr.position;

            if (lastPosition.x != position.x || lastPosition.z != position.z)
            {
                ani.SetBool("Run", true);
                var dirrection = position - lastPosition;
                dirrection.y = 0;
                lastRotation = Quaternion.LookRotation(dirrection);
            }
            else
            {
                ani.SetBool("Run", false);
            }
            transform.rotation = Quaternion.Lerp(transform.rotation, lastRotation, 10f * Time.deltaTime);
            lastPosition = position;
        }
    }

    public void SetMovingState(bool val)
    {
        ani.SetBool("Run", val);
    }

    public void Attack()
    {
        ani.SetTrigger("Attack");
    }

    public bool IsMoving
    {
        get { return ani.GetBool("Run"); }
    }
}
