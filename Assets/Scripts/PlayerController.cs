﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class PlayerController : MonoBehaviour
{    
    private Vector3 playerVelocity;
    private bool groundedPlayer;
    private float playerSpeed;

    private Animator ani;
    private CharacterController character;

    private float jumpHeight = 2.0f;
    private float gravityValue = -9.81f;

    public CinemachineVirtualCamera CharacterCamera;
    public CinemachineVirtualCamera AimCamera;
    public CinemachineVirtualCamera DanceCamera;

    void Start()
    {        
        ani = gameObject.GetComponent<Animator>();        
        character = gameObject.GetComponent<CharacterController>();        
    }

    // Update is called once per frame
    void Update()
    {        
        groundedPlayer = character.isGrounded;

        if (groundedPlayer && playerVelocity.y < 0)
        {
            playerVelocity.y = -0.1f;
        }

        Vector3 move = Vector3.zero;

        if (!ani.GetBool("Dance"))
        {            
            move = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
            character.Move(move * Time.deltaTime * playerSpeed);
        }

        if (move != Vector3.zero)
        {
            gameObject.transform.forward = move;
            if (playerSpeed < 1)
            {
                ani.SetFloat("Speed", playerSpeed);
                playerSpeed += 0.0025f;
            }
            if (!ani.GetBool("Crouching"))
            {                
                if (playerSpeed < 8)
                {
                    playerSpeed += 0.01f;
                }
            }
            else
            {
                if (playerSpeed > 2)
                {
                    playerSpeed = 2;
                }
                else
                {
                    playerSpeed += 0.01f;
                }
            }
        }        
        else if (playerSpeed > 1)
        {
            playerSpeed = 1;            
        }
        else if (playerSpeed > 0 && playerSpeed <= 1)
        {
            playerSpeed -= 0.005f;
            ani.SetFloat("Speed", playerSpeed);
        } else if (playerSpeed < 0)
        {
            playerSpeed = 0;
            ani.SetFloat("Speed", playerSpeed);
        }

        if (Input.GetKeyDown(KeyCode.Q) && !ani.GetBool("Dance"))
        {            
            StartCoroutine(Dance());
        }

        ani.SetBool("Crouching", Input.GetKey(KeyCode.LeftControl));

        if (Input.GetKeyDown(KeyCode.Space) && !ani.GetBool("Dance") && groundedPlayer)
        {            
            ani.Play("Jump");
            playerVelocity.y += Mathf.Sqrt(jumpHeight * -3.0f * gravityValue);
        }

        if (Input.GetKeyDown(KeyCode.Mouse1) && !ani.GetBool("Dance"))
        {
            ani.SetBool("RifleWalk", !ani.GetBool("RifleWalk"));
            ani.SetBool("Aim", !ani.GetBool("Aim"));
            AimCamera.gameObject.SetActive(ani.GetBool("Aim"));
            CharacterCamera.gameObject.SetActive(!ani.GetBool("Aim"));
        }

        if (Input.GetKeyDown(KeyCode.E) && !ani.GetBool("Dance"))
        {            
            StartCoroutine(Pick());
        }
    }

    IEnumerator Dance()
    {
        ani.SetLayerWeight(2, 1f);
        ani.SetBool("Dance", true);
        CharacterCamera.gameObject.SetActive(false);
        DanceCamera.gameObject.SetActive(true);
        yield return new WaitForSeconds(15);
        ani.SetBool("Dance", false);
        CharacterCamera.gameObject.SetActive(true);
        DanceCamera.gameObject.SetActive(false);
        ani.SetLayerWeight(2, 0f);
    }
    
    IEnumerator Pick()
    {        
        ani.SetBool("Dance", true);
        ani.SetTrigger("Pick");
        yield return new WaitForSeconds(3);
        ani.SetBool("Dance", false);
    }

    private void OnTriggerStay(Collider other)
    {        
        if (Input.GetKey(KeyCode.E) && other.gameObject.CompareTag("Object"))
        {
            other.gameObject.SetActive(false);
            Debug.Log("Item picked up");
        }
    }
}
